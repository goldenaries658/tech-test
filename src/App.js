import { Editor } from "./modules";

function App() {
  return (
    <>
      <Editor />
    </>
  );
}

export default App;
