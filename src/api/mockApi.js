import mockData from "./assets/mockData.json";

// A mock function to mimic making an async request for data
export function fetchData() {
  return new Promise((resolve) =>
    setTimeout(() => resolve({ data: { mockData } }), 500)
  );
}
