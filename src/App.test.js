import { fetchData } from "./api/mockApi";

test("fetches correct data", () => {
  return fetchData().then(({ data: { mockData } }) => {
    expect(mockData).toEqual({
      fieldDefinitions: [
        "Domain",
        "Object",
        "ObjectClass",
        "Free Text",
        "DateTime",
        "Origin",
        "OriginEvtClass",
        "DomainClass",
        "OriginSeverity",
        "ObjectState",
        "Name",
        "AlertType",
        "hasAccepted",
        "hasAcknowledged",
      ],
      title: "The Editor",
    });
  });
});
