import { useEffect, useState } from "react";
import { fetchData } from "../../api/mockApi";

import "./Editor.scss";

export default function Editor() {
  const [state, setState] = useState({
      title: "",
      fieldDefinitions: [],
      selected: [],
    }),
    [highlightedOption, setHighlightedOption] = useState("");

  const { title, fieldDefinitions, selected } = state;

  useEffect(() => {
    const getData = async () => {
      try {
        const {
          data: { mockData },
        } = await fetchData();

        setState({ ...mockData, selected: [] });
      } catch (err) {
        console.error(`Error: Editor.js - getData(): ${err}`);
      }
    };

    getData();
  }, []);

  const handleClick = (e) => {
    const { innerText } = e.target;

    setHighlightedOption(highlightedOption === innerText ? "" : innerText);
  };

  const removeDefinition = (arr) =>
    arr.filter((definition) => definition !== highlightedOption);

  const moveDefinitionTo = (target) => {
    if (highlightedOption) {
      switch (target) {
        case fieldDefinitions:
          !fieldDefinitions.includes(highlightedOption) &&
            setState({
              ...state,
              fieldDefinitions: [...fieldDefinitions, highlightedOption],
              selected: removeDefinition(selected),
            });
          break;

        case selected:
          !selected.includes(highlightedOption) &&
            setState({
              ...state,
              fieldDefinitions: removeDefinition(fieldDefinitions),
              selected: [...selected, highlightedOption],
            });
          break;

        default:
          break;
      }

      setHighlightedOption("");
    }
  };

  return (
    <div className="container">
      <h1 className="title">{title}</h1>

      <div className="card-body">
        <div className="definitions list-container">
          <h3>Definitions</h3>
          <ul className="definitions-list">
            {fieldDefinitions.map((definition) => (
              <li
                key={definition}
                className={
                  definition === highlightedOption ? "highlighted" : undefined
                }
                onClick={handleClick}
              >
                {definition}
              </li>
            ))}
          </ul>
        </div>

        <div className="action-btns">
          <button
            className="use-btn"
            onClick={(e) => {
              moveDefinitionTo(selected);
            }}
          >
            {"use >"}
          </button>
          <button
            className="remove-btn"
            onClick={(e) => {
              moveDefinitionTo(fieldDefinitions);
            }}
          >
            {"< remove"}
          </button>
        </div>

        <div className="selected list-container">
          <h3>Selected</h3>
          <ul className="selected-list">
            {selected.map((definition) => (
              <li
                key={definition}
                className={
                  definition === highlightedOption ? "highlighted" : undefined
                }
                onClick={handleClick}
              >
                {definition}
              </li>
            ))}
          </ul>
        </div>
      </div>
    </div>
  );
}
